# -----------------------------------------------------------------------------
# flucohplc startup cmd
# -----------------------------------------------------------------------------
require flucohplc

# -----------------------------------------------------------------------------
# setting parameters when not using auto deployment
# -----------------------------------------------------------------------------
epicsEnvSet(PORTNAME, "PortA")
epicsEnvSet(IPADDR, "192.168.1.32")
epicsEnvSet(IPPORT, "4001")
epicsEnvSet(LOCATION, "Utgard; $(IPADDR)")
epicsEnvSet(P, "utg-flucohplc-001")
epicsEnvSet(IOCNAME, "utg-flucohplc-001")
epicsEnvSet(STREAM_PROTOCOL_PATH, "$(flucohplc_DIR)db")

# -----------------------------------------------------------------------------
# loading databases
# -----------------------------------------------------------------------------
#

# flucohplc
iocshLoad("$(flucohplc_DIR)/flucohplc.iocsh")
# EOF
